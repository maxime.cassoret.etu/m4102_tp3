## API et représentation des données

API REST ressource *pizza*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /pizzas     | récupère l'ensemble des pizzas                | 200 et un tableau de pizza                    |
| GET       | /pizzas/{id}| récupère la pizza d'identifiant id            | 200 et la pizza                               |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /pizzas/{id}/name | récupère le nom de la pizza    | 200 et le nom de la pizza                              |
|           |             | d'identifiant id                              | 404 si id est inconnu                         |
| POST      | /pizzas | création de pizza                     | 201 et l'URI de la ressource créée + représentation       |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la pizza existe déjà (même nom)        |
| DELETE    | /pizzas/{id} | destruction de la pizza d'identifiant id | 204 si l'opération à réussi                       |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /pizza/ingredient/{id} | recuperes toute les pizzas qui posséde cette ingrédients | 200 et un tableau de pizza |
| 	        | 			     | 		       	   	      	  | 404 si l'ingrédients n'éxiste pas |
| GET 	    | /pizza/{id}/ingredient | recuperes tout les ingrédients de la pizza d'identifiant id | 200 et un tableau d'ingredients |
| 	        | 			     | 		      	  	      	  | 404 si id est inconnu


Une pizza comporte un identifiant et un nom ainsi que ses ingrédients. Sa
représentation JSON prendra donc la forme suivante :

    {
	  "id": 1,
	  "name": "Reine"
	  "ingredients" : ["mozzarella","lardon","champignon"]
    }
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Aussi on aura une
représentation JSON qui comporte uniquement le nom et les ingrédients:

    {
	"name": "Reine"
	"ingredients": ["mozzarella","lardon","champignon"]
    }
