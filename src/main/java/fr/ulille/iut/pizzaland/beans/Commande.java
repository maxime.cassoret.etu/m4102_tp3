package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.Arrays;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private long id;
	private String nom;
	private String prenom;
	private Pizza[] pizzas;

	public Commande() {
	}
	
	public Commande(long id, String nom, String prenom, Pizza[] pizzas) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.pizzas = pizzas;
	}

	public void setId(long id) {
		this.id = id;
	}	

	public long getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Pizza[] getPizzas() {
		return pizzas;
	}

	public void setPizzas(Pizza[] pizzas) {
		this.pizzas = pizzas;
	}

	public static CommandeDto toDto(Commande c) {
		CommandeDto dto = new CommandeDto();
		dto.setId(c.getId());
		dto.setNom(c.getNom());
		dto.setPrenom(c.getPrenom());
		dto.setPizzas(c.getPizzas());
		
		return dto;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande commande = new Commande();
		commande.setId(dto.getId());
		commande.setNom(dto.getNom());
		commande.setPrenom(dto.getPrenom());
		commande.setPizzas(dto.getPizzas());

		return commande;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (!Arrays.equals(pizzas, other.pizzas))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

	public static CommandeCreateDto toCreateDto(Commande commande) {
		CommandeCreateDto dto = new CommandeCreateDto();
		dto.setNom(commande.getNom());
		dto.setPrenom(commande.getPrenom());
		
		ArrayList<Long> list = new ArrayList<>();
		for (Pizza i : commande.getPizzas()) {
			list.add(i.getId());
		}
		
		dto.setPizzas(list.toArray(new Long[list.size()]));

		return dto;
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
		PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		Commande pizza = new Commande();
		pizza.setNom(dto.getNom());
		pizza.setPrenom(dto.getPrenom());

		ArrayList<Pizza> list = new ArrayList<>();
		for (Long l : dto.getPizzas()) {
			list.add(pizzaDao.findById(l));
		}
		
		pizza.setPizzas(list.toArray(new Pizza[list.size()]));

		return pizza;
	}

	@Override
	public String toString() {
		return "Commande [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", pizzas=" + Arrays.toString(pizzas)
				+ "]";
	}

	
	
}