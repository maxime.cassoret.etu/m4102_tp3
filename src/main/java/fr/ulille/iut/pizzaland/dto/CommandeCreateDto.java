package fr.ulille.iut.pizzaland.dto;

import java.util.Arrays;

public class CommandeCreateDto {
	private String nom;
	private String prenom;
	private Long[] pizzas;
	
	public CommandeCreateDto() {
		
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Long[] getPizzas() {
		return pizzas;
	}

	public void setPizzas(Long[] pizzas) {
		this.pizzas = pizzas;
	}

	@Override
	public String toString() {
		return "CommandeCreateDto [nom=" + nom + ", prenom=" + prenom + ", pizzas=" + Arrays.toString(pizzas) + "]";
	}

	
	
	
	
}
