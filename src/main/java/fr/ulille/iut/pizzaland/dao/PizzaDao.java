package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

  
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizza (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	  void createPizzaTable();


	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientAssociation(idIngredient INTEGER, idPizza INTEGER, constraint pk_pizzaIA Primary key(idIngredient,idPizza),FOREIGN KEY(idIngredient) references fk_piz,FOREIGN KEY(idPizza) references fk_piz)")
	  void createAssociationTable();

  default void createPizzaAndAssociationTable(){
      createPizzaTable();
      createAssociationTable();
  }

  @SqlUpdate("DROP TABLE IF EXISTS pizza")
  void dropTable();

  @SqlUpdate("INSERT INTO pizza (name) VALUES (:name)")
  @GetGeneratedKeys
  long insert(String name);

  @SqlQuery("SELECT * FROM pizza")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll();

  @SqlQuery("SELECT * FROM pizza WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(long id);
  
  @SqlQuery("SELECT * FROM pizza WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(String name);

  @SqlUpdate("DELETE FROM pizza WHERE id = :id")
  void remove(long id);

}